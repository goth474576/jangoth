'''
Navrhnete strukturu a funkcionalitu pro system udrzujici jizdni rady
Pole (list) bude predstavovat (databazi jizdnich radu) a jizdni rady budou představeny
slovnikem, ktery bude obsahovat nasledujici vlastnosti:
  ID trasy: integer, např. 1. Každé trase vyberte identické číslo
  misto odjezdu: nazev mesta
  misto prijezdu: nazev jineho mesta
  cas odjezdu: ve formatu HH:mm (napr. 18:00) typu retezec ('18:00'). Kdo chce muze vyuzit datovy typ datetime (https://docs.python.org/3/library/datetime.html)
  cas prijezdu: obdobne jako cas odjezdu
  typ dopravního prostředku: řetězec
  vzdalenost: vzdalenost mezi mesty v km
  kilometrovou sazbu: cislo typu float (napr. 1.50)

Celý slovník představující linku by mohl vypadat nějak takto:
{'id': 1, 'odjezd_mesto': 'Brno', 'prijezd_mesto': 'Praha', 'odjezd_cas': '7:00', 'prijezd_cas': '9:30', 'prostredek': 'autobus', 'vzdalenost': 200, 'sazba': 1.50}

Systém bude umět (znamená definujte funkce, které budou dělat) následující:
1. Zadat do systému jednu novou trasu - pokud uzivatel zada trasu se stejným ID, vypište hlášku, že zadaná trasa již existuje trasu do pole nepřidejte
2. Zadat do systému více nových tras najednou
3. Vypsat vsechny informace o trase, podle ID
4. Vypište trasy, které jsou obsluhovány dopravním prostředkem, který uživatel zadá
5. Vypočítejte cenu cesty, kterou uzivatel bude chtit spocitat (podle ID). Použijte vzdalenost a kilometrovou sazbu
6. Vymazani trasy podle zadaneho ID. Uzivatel napriklad zada ze chce vymazat trasu s ID 5 a vy danou trasu vymazete
7. Vypocitejte kolik minut dana linka jede. Najdete funkci, ktera vam cas rozdeli podle dvojtecky cimz ziskate hodiny a minuty zvlast. Z tech se pokuste vypocitat cas cesty v minutach

Na úvod si v kódu systém naplňte několika trasama ať je nemusíte vždycky vyplňovat ručně
Celému návrhu ponechávám volnou ruku. Je na vás jak navrhnete a naprogramujete systém, ale pokuste se ho nejak naprogramovat
Správnost řešení pak můžeme probrat osobně

PORADNE SI PRECTETE ZADANI A PROMYSLETE HO


Pokud si s něčím nebudete ani po týdenním zoufalství vůbec vědět rady, tak udělejte aspoň to co znáte a na zbytek se klidně zeptejte přes mail nebo osobně na příští hodině.
'''
import pprint
import datetime

jizdni_rad = [{'id': 1, 'odjezd_mesto': 'Brno', 'prijezd_mesto': 'Praha', 'odjezd_cas': '7:00', 'prijezd_cas': '9:30', 'prostredek': 'autobus', 'vzdalenost': 200, 'sazba': 1.50},
              {'id': 2, 'odjezd_mesto': 'Jihlava', 'prijezd_mesto': 'Brno', 'odjezd_cas': '6:40', 'prijezd_cas': '8:30', 'prostredek': 'vlak', 'vzdalenost': 101, 'sazba': 0.50}]

def nova_trasa():
    nt = {}
    id1 = int(input('Zadej ID linky: '))

    for linky in jizdni_rad:
        if linky['id'] == id1:
            print('Tato trasa již existuje.')
            menu()
        else:
            nt['id'] = id1

    odjezd_mesto = input('Zadej obec, ze které linka vyjíždí: ')

    nt['odjezd_mesto'] = odjezd_mesto

    prijezd_mesto = input('Zadej obec, ve které linka končí: ')

    nt['prijezd_mesto'] = prijezd_mesto

    odjezd_cas = input('Zadej čas odjezdu (např. 7:00): ')

    nt['odjezd_cas'] = odjezd_cas

    prijezd_cas = input('Zaej čas příjezdu (např. 15:00): ')

    nt['prijezd_cas'] = prijezd_cas

    prostredek = input('Zadej prostředek: ')

    nt['prostredek'] = prostredek

    vzdalenost = int(input('Zadej vzdálenost trasy (v km): '))

    nt['vzdalenost'] = vzdalenost

    sazba = int(input('Zadej sazbu na 1 km: '))

    nt['sazba'] = sazba

    jizdni_rad.append(nt)

def info_id():
    idvstup = int(input('Zadej ID linky: '))

    for linky in jizdni_rad:
        if linky['id'] == idvstup:
            pprint.pprint(linky)
        else:
            pass

def info_prostredek():
    prostredekvstup = input('Zadej prostředek: ')

    for linky in jizdni_rad:
        if linky['prostredek'] == prostredekvstup:
            print(linky)
        else:
            pass

def cena_cesty():
    idvstup = int(input('Zadej ID linky: '))

    for linky in jizdni_rad:
        if linky['id'] == idvstup:
            cena = linky['vzdalenost'] * linky['sazba']
            print(cena, 'Kč')
        else:
            pass

def smazat_trasu():
    idvstup2 = int(input('Zadej ID linky: '))

    for linky in range(len(jizdni_rad)):
        if jizdni_rad[linky]['id'] == idvstup2:
            del jizdni_rad[linky]
            print(jizdni_rad)
            menu()
        else:
            pass

def cas_cesty():
    idvstup3 = int(input('Zadej ID linky: '))

    for linky in jizdni_rad:
        if linky['id'] == idvstup3:
            odjezd = datetime.datetime.strptime(linky['odjezd_cas'], '%H:%M')
            prijezd = datetime.datetime.strptime(linky['prijezd_cas'], '%H:%M')
            rozdil = prijezd - odjezd
            print(rozdil.total_seconds()/60, 'minut')
        else:
            pass

def menu():
    print(jizdni_rad)
    vstup = int(input('Vyberte jednu z možností:\n'
                       '1: Zadat do systému novou trasu.\n'
                       '2: Zadat do systému více nových tras najednou. TO SE MUSÍ ZADAT V KÓDU - Line 170.\n'
                       '3: Vypsat všechny informace o vybrané trase.\n'
                       '4: Vypsat informace o trase podle prostředku.\n'
                       '5: Vypočítat cenu vybrané trasy.\n'  
                       '6: Vymazat vybranou trasu.\n'
                       '7: Vypočítat časovou vzdálenost vybrané linky.\n'
                        '8: Ukončit program.\n'))
    if vstup == 1:
        nova_trasa()
    elif vstup == 2:
        print('Tahle funkce nefunguje.')
    elif vstup == 3:
        info_id()
    elif vstup == 4:
        info_prostredek()
    elif vstup == 5:
        cena_cesty()
    elif vstup == 6:
        smazat_trasu()
    elif vstup == 7:
        cas_cesty()
    elif vstup == 8:
        quit()

def nove_trasy(*args):
    for linky2 in jizdni_rad:
        for arg in args:
            if linky2['id'] == arg['id']:
                print('Trasa s id = ', arg['id'], 'již existuje.')
            else:
                jizdni_rad.append(arg)
        break

#nove_trasy({'id': 8, 'odjezd_mesto': 'Jihlava', 'prijezd_mesto': 'Brno', 'odjezd_cas': '6:40', 'prijezd_cas': '8:30', 'prostredek': 'vlak', 'vzdalenost': 101, 'sazba': 0.50}, {'id': 10, 'odjezd_mesto': 'Jihlava', 'prijezd_mesto': 'Brno', 'odjezd_cas': '6:40', 'prijezd_cas': '8:30', 'prostredek': 'vlak', 'vzdalenost': 101, 'sazba': 0.50})

while True:
    menu()

